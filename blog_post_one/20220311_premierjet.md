# How can Service Design improve Legal Service?

Many lawyers these days are making efforts to be more competitive in pricing and to develop healthier client-firm relationships. 

In this post, I argue that Law Firms that want to invest in operations ('legal ops' for acronym lovers🤓), and transform their service could be looking at Service Design, a methodology that has been there for more than 20 years and that [we have been practicing for quite while now](https://www.linkedin.com/feed/update/urn:li:activity:6889885593061400576/). 

## What is Design Thinking? 
Design Thinking is a thinking process that comes from "product" designers. Basically the the people designing the mouse 🖱️and computers follow these thinking processes to build useful products that people love. This type of thinking is **what sets appart good designed products** from poorly designed products. You get the picture right? 

From products, these thinking methods moved to the design of services which are immaterial more relational interactions between people and companies. 

This is one of my favorite quotes from [Herbert Simon](https://en.wikipedia.org/wiki/Herbert_A._Simon) a political scientist from the 60s who was the first person to come up with the term Design Thinking.  

> Everyone designs who devises course of action aimed at changing existing situations into preferred ones.  

I love this quote for two reasons :

* first because it opens up the methodology to everyone and not just the product designers. 
* And second, [As Diana Pinos perfectly states in her blog](https://dianapinos.com/2019/05/14/que-empresas-utilizan-design-thinking/), _Design Thinking goes much further than "product", to solve all kinds of situations in an organization: new products, services, ways of communicating, how you treat your employees, ways of working, etc.*

Design Thinking is a mindset and a set of tools that are going to make you stand out from our competitors. It's based on __deeper understanding the experience of the client__ and a deeper understanding of the broader ecosystem gravitating around your firm. 

## What is Service Design

The goal is to **restructure your organization** (teams, tech and processes) so that it adapts to offer a better experience for all the actors involved. I know this may sound a bit is a bit abstract but it's because it is. But The vast majority of what goes into a service is essentially invisible. Agree?!

With Service Design **you need to go "meta"** and get the big picture of how are you actually delivering your service as a system. You need to identify and visualize where are the points that need to be improved. 

Service Design includes things like:

* **visualization** tools allow bringing different technical disciplines together and 
* **meeting facilitation** techniques 
* a step by step process to innovate: create a new service or re-organize an existing one 
* **research methods** to discover insights about your audience and other stakeholder's 
* create regular communication "avenues" to include your client in the design of your legal operations 

## Service Design can improve a legal service 
If you are into streamlining your service so that you can **repeat same processes with new clients and boost profitability**, this can be a game changer. 

You will be able to clearly see bottlenecks back end (hidden processes) and front end (what customer sees).  Come up with ideas to reduce the amount of time you spend on certain tasks.

Current business management in the digital age is a complex matter: Service design promises to be great method to manage and visualize complexity.

##  What are the skills you need? 

Service Design is optimistic. Service designers really believe that **the perfect service is possible to achieve** if you are willing to dream it, imagine it and co-create it with everyone involved. 

You need people to be able to work in different "modes". This requires to use brain functions that often are not used in legal profession:

* During the service design process there is a time for ideation in which you can be creative and "brainstorm". 
* There is a time for strategy and organizing the way forward. 
* Collaboration skills. understanding people working from different disciplines. talk with people that are from other disciplines.  
* Tools to visualize, sketchnote and draw.

###  Seeing operations through your customer's eyes

The ability to **listen and to extract user needs** from human interactions will help you succeed as a lawyer. Service design believes that the user is also an expert in his/her life. What they see and how they speak about their problems can give you great insights on how to improve your legal operations. Design thinking includes also the left part of the brain so emotions are also taken on board in the design of the perfect service. 

## What are the challenges?

It's an innovation process right? And let's be honest, lawyers have not been trained to innovate. I say it from my own experience as trained lawyer. 

This means you are going to **spend some time working on things that are very uncertain** and that you don't know the outcome. If your team does not have an innovation mindset, or the mental resilience to trust this process, this is going to generate frustration and stress.

Also make sure you explain very well the innovation journey. What are we going to be doing at each phase because it's very easy for people to get lost. 

Ok, enough buzz words and theoretical concepts🤯.  As I always say, 
_Design is like Yoga. It's about doing it and not talking about it.  _

So now it's time to get our hands dirty. Let's start including design in our practice.👇

We have worked with Law Firms starting their design challenge and we would like to help you . [Apply for a free consultation call](https://bigkidscontent.com/contact/) and let's see if together if we come up with a plan to start this exciting journey.  





